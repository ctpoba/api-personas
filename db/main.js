var path = require('path');
var Datastore = require('@seald-io/nedb')

const bases = ['usuarios', 'personas']
var db =  {};

bases.forEach((base) => {
  db[base] = new Datastore({ filename: path.join(__dirname, `${base}.db`) });
})

Object.keys(db).forEach((collection) => {
  db[collection].loadDatabase(function (error) {
    if (error) return console.log(error);
    return console.log(`Base de datos ${collection} iniciada`);
  });
})

module.exports = db;