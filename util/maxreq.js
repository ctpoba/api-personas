const router = require('express').Router()

// let lista_bloqueados = require('./bloqueados.json');
const BAN = require('./bloqueados.json');
const MAX_REQ = 20;
const INTERVAL = 0.5 * 60 * 1000;

let lista_bloqueados = [...BAN];
let lista_clientes = {};


setInterval(() => {
  // console.log("limpio");
  lista_bloqueados = [...BAN];
  lista_clientes = {};
}, INTERVAL)


router.use(function(req, res, next){
  const add = req.socket.remoteAddress;
  if (lista_bloqueados.findIndex((item, i) => item === add) !== -1) {
    // console.log(`bloqueado: ${add}`);
    return res.json({status: "bloqueado"});
  }
  if (lista_clientes[add] === undefined) lista_clientes[add] = 1;
  else lista_clientes[add] += 1;
  if (lista_clientes[add] >= MAX_REQ) {
    console.log(`Bloqueado ${add}`);
    lista_bloqueados.push(add);
    return res.json({status: "bloqueado"});
  }
  // console.log({lista_clientes, lista_bloqueados});
  // console.log({address: add});
  // console.log({address: req.socket.remoteAddress, port: req.socket.remotePort});
  next();
})

router.get("/bloqueados",function(req, res, next){
  res.json({ status:"ok", data:{lista_clientes, lista_bloqueados} })
})

module.exports = router;