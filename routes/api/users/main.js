const router = require('express').Router();

const { registrar, ingresar, checkToken, obtenerUsuarios } = require('./assets/funciones');

router.post('/ingresar', function(req, res, next){
  const {user, pass} = req.body;
  ingresar(user, pass)
  .then(({user, token}) => {
    res.json({ status:"ok", token, user })
  })
  .catch((error) => {
    console.log(error);  
    res.json({ status:"error", error }) 
  })
})

router.post('/registrar', function(req, res, next){
  registrar(req.body)
  .then((newUser) => {
    console.log(newUser);
    res.json({ status: "ok", user_id: newUser._id })
  })
  .catch((error) => {
    console.log(error);
    res.json({ status:"error", error }) 
  })
})


router.use('/private', function(req, res, next){
  const {authorization} = req.headers;
  checkToken(authorization)
  .then((user) => {
    req.user = user;
    next();
  })
  .catch((error) => {
    console.log(error);  
    res.json({ status:"error", error }) 
  })
})

router.get('/private/lista', function(req, res, next){
  obtenerUsuarios()
  .then((usuarios) => {
    let newUsers = usuarios.map((user, i) => {
      let newUser = {...user};
      delete newUser.pass;
      delete newUser.token;
      return newUser
    });
    res.json({ status: "ok", usuarios: newUsers});
  })
  .catch((error) => {
    console.log(error);
    res.json({ status: "error", error });
  })
})


module.exports = router