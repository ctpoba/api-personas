const {usuarios} = require('../../../../db/main')

const CAMPOS_OBLIGATORIOS = ['user', 'pass', 'nombres', 'apellidos', 'documento']
const TODOS_CAMPOS = ['user', 'pass', 'nombres', 'apellidos', 'documento', 'telefono', 'domicilio', 'mail']

const validarDatos = function(datos, obligatorio = true){
  let flag = true;
  if (obligatorio){
    for (let i = 0; i < CAMPOS_OBLIGATORIOS.length; i++) {
      const valor = datos[CAMPOS_OBLIGATORIOS[i]];
      if (valor === undefined || valor === null || valor.length === 0) flag = false;
    }
  }
  const campos = Object.keys(datos);
  for (let i = 0; i < campos.length; i++) {
    if (!TODOS_CAMPOS.includes(campos[i])) {
      console.log({campo: campos});
      flag = false
    };
  }
  return flag;
}

const newCodigo = function(length){
  const characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const charactersLength = characters.length;
  let result = '';
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const getToken = function(){
  return newCodigo(16); 
}

const getUsuario = function(user, documento = null, pass = null){
  return new Promise((resolve, reject) => {
    let filter = {};
    if (documento !== null) filter = {$or: [{documento}, {user}]};
    if (pass !== null) filter = {$and: [{pass}, {user}]};
    usuarios.findOne(filter, function (error, docs) {
      if (error) return reject(error);
      console.log(docs);
      return resolve(docs)
    });
  })
}

const ingresar = function(user, pass){
  return new Promise((resolve, reject) => {
    getUsuario(user, null, pass)
    .then((user) => {
      if (user !== null){
        const token = getToken();
        usuarios.updateAsync({_id: user._id}, {$set: {token}})
        .then(() => {
          delete user.pass;
          delete user.token;
          return resolve({user, token})
        })
      } else {
        return reject("No coincide usuario/contraseña");
      }
    })
    .catch((error) => reject(error));
  })
}

const registrar = function(newUser){
  return new Promise((resolve, reject) => {
    getUsuario(newUser.user, newUser.documento)
    .then((existUser) => {
      if (existUser === null){
        if (validarDatos(newUser)){
          usuarios.insert(newUser, function(error, doc){
            if (error) return reject(error);
            return resolve(doc)
          })
        } else {
          reject("Datos obligatorios incompletos o datos no validos");
        }
      } else {
        return reject("Usuario ya registrado");
      }
    })
    .catch((error) => reject(error));
  })
}

const checkToken = function(token){
  return new Promise((resolve, reject) => {
    usuarios.find({ token }, function(error, users) {
      if (error) return reject(error);
      if (users.length === 1) return resolve(users[0]);
      else return reject("Error Token incorrecto ");
    })
  })
}

const guardarToken = function(userId, token){
  return new Promise((resolve, reject) => {
    usuarios.update({ _id: userId }, { $set: { token } }, { multi: false }, function (error) {
      if (error) return reject(error);
      return resolve();
    });
  })
}

const obtenerUsuarios = function(){
  return new Promise((resolve, reject) => {
    usuarios.find({}, function(error, usuarios) {
      if (error) return reject(error);
      return resolve(usuarios);
    })
  })
}

module.exports = { ingresar, registrar, checkToken, guardarToken, getToken, obtenerUsuarios }