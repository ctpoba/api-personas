const router = require('express').Router();

const personasRouter = require('./personas/main')
const usersRouter = require('./users/main')


router.use(usersRouter);
router.use('/personas', personasRouter);

module.exports = router;