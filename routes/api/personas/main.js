const router = require('express').Router();
const { guardarPersona, buscarPersonas, actualizarPersona, eliminarPersona } = require('./assets/funciones');
const {checkToken} = require('../users/assets/funciones')

/*
  POST "/api/personas" {documento, nombres, apellidos, fechaNac[, telefono, domicilio, mail]} => {status, persona_id}
  PUT "/api/personas" persona_id & {documento, nombres, apellidos, fechaNac[, telefono, domicilio, mail]} => {status}
  GET "/api/personas" busqueda (dni, apellidos, nombres) => {status, [{persona_id, documento, nombres, apellidos, fechaNac, telefono, domicilio, mail}]}
  DELETE "/api/personas" persona_id => {status}
*/

//MIDDLEWARE
router.use("/", function(req, res, next){
  const {authorization} = req.headers;
  checkToken(authorization)
  .then((user) => {
    req.user = user;
    next();
  })
  .catch((error) => {
    console.log(error);  
    res.json({ status:"error", error }) 
  })
})

router.post("/", function(req, res, next){
  guardarPersona(req.body)
  .then((persona_id) => {
    res.json({ status:"ok", persona_id })
  })
  .catch((error) => {
    console.log(error);
    res.json({ status:"error", error})
  })
})

router.put("/", function(req, res, next){
  const {persona_id} = req.query;
  actualizarPersona(persona_id, req.body)
  .then(() => {
    res.json({ status:'ok' })
  })
  .catch((error) => {
    console.log(error);
    res.json({ status:"error", error})
  })
})

router.get("/", function(req, res, next){
  const {busqueda} = req.query;
  buscarPersonas(busqueda)
  .then((personas) => {
    res.json({ status:'ok', personas })
  })
  .catch((error) => {
    console.log(error);
    res.json({ status:"error", error})
  })
})

router.delete("/", function(req, res, next){
  const {persona_id} = req.query;
  eliminarPersona(persona_id)
  .then(() => {
    res.json({ status:'ok' })
  })
  .catch((error) => {
    console.log(error);
    res.json({ status:"error", error})
  })
})

module.exports = router;