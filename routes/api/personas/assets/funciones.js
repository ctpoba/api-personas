const {personas} = require('../../../../db/main')

const CAMPOS_OBLIGATORIOS = ["documento", "nombres", "apellidos", "fechaNac"]
const TODOS_CAMPOS = ["documento", "nombres", "apellidos", "fechaNac", "telefono", "domicilio", "mail"]

const validarDatos = function(datos, obligatorio = true){
  let flag = true;
  if (obligatorio){
    for (let i = 0; i < CAMPOS_OBLIGATORIOS.length; i++) {
      const valor = datos[CAMPOS_OBLIGATORIOS[i]];
      if (valor === undefined || valor === null || valor.length === 0) flag = false;
    }
  }
  const campos = Object.keys(datos);
  for (let i = 0; i < campos.length; i++) {
    if (!TODOS_CAMPOS.includes(campos[i])) flag = false;
  }
  return flag;
}

const buscarPersonas = function(busqueda){
  return new Promise((resolve, reject) => {
    let filtros = {}
    if (busqueda !== undefined && busqueda !== null){
      const busquedaExp = new RegExp(busqueda)
      filtros = {$or: [ {documento: busquedaExp}, {apellidos: busquedaExp}, {nombres: busquedaExp} ]}
    }
    personas.findAsync(filtros)
    .then((personas) => resolve(personas))
    .catch((error) => reject(error));
  })
}

const existePersona = function(tipo, dato, busca=false){
  return new Promise((resolve, reject) => {
    personas.findAsync({[tipo]:dato}, {_id:1})
    .then((personas) => {
      if (busca) {
        if (personas.length === 1) return resolve();
        return reject("La persona no se encuentra registrada");
      } else {
        if (personas.length === 0) return resolve();
        return reject("La persona ya se encuentra registrada");
      }
    })
    .catch((error) => reject(error));
  })
}

const guardarPersona = function(datos){
  return new Promise((resolve, reject) => {
    if (validarDatos(datos)){
      existePersona('documento', datos.documento)
      .then(() => {
        personas.insertAsync(datos)
        .then((persona) => resolve(persona._id))
        .catch((error) => reject(error));
      })
      .catch((error) => reject(error))
    } else {
      reject("Datos obligatorios incompletos o datos no validos");
    }
  })
}

const actualizarPersona = function(persona_id, datos){
  return new Promise((resolve, reject) => {
    existePersona('_id', persona_id, true)
    .then(() => {
      if (validarDatos(datos, false)){
        personas.updateAsync({_id: persona_id}, {$set: datos})
        .then(() => resolve())
        .catch((error) => reject(error));
      } else {
        reject("Datos obligatorios incompletos o datos no validos");
      }
    })
    .catch((error) => reject(error))
  })
}

const eliminarPersona = function(persona_id){
  return new Promise((resolve, reject) => {
    existePersona('_id', persona_id, true)
    .then(() => {
      personas.removeAsync({_id:persona_id}, {})
      .then(() => resolve())
      .catch((error) => reject(error))
    })
    .catch((error) => reject(error))
  })
}

module.exports = { guardarPersona, buscarPersonas, actualizarPersona, eliminarPersona };