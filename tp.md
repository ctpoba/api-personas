Diseñar y desarrollar una app web para permitir registrar usuarios, iniciar sesión y gestionar personas.

La aplicación debe identificar si el usuario inició sesión o no, para que en caso de no haberlo hecho todavía, se muestre el formulario para registrar y el formulario para ingresar.
Una vez haya iniciado sesión, debe mostrarle las herramientas para gestionar las personas.
La gestión de personas debe permitir agregar, listar, editar y eliminar a cada una.

